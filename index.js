/**
 * @file
 * HTTP Server is defined in here.
 *
 * In this file, logger, body parser, and router is injected
 * to the HTTP server as middleware. Where the logger only active
 * if only the server is not run inside test environment.
 */

/**
 * Dependencies */
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const router = require("./router");

/**
 * Fetch variable from process environment variable */
const { NODE_ENV = "development" } = process.env;

/**
 * Initialize Express Server Instance */
const app = express();

/**
 * Inject logger middleware */
if (NODE_ENV != "test") app.use(morgan("dev"));

/**
 * Inject request body JSON parser */
app.use(express.json());

/**
 * Inject cors middleware */
app.use(cors());

/**
 * Inject router to HTTP Server */
app.use(router);

module.exports = app;
