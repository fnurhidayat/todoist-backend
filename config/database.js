/**
 * @file
 * Database configuration.
 *
 * Define database connection information,
 * telling the sequelize to connect to which database.
 */

/**
 * Fetch variable from process environment variable */
const {
  // Port that the database listening to
  DATABASE_PORT = 5432,

  // Database name
  DATABASE_NAME = "todoist",

  // Database username
  DATABASE_USERNAME = null,

  // Database password
  DATABASE_PASSWORD = null,

  // Database host
  DATABASE_HOST = "localhost",
} = process.env;

module.exports = {
  development: {
    username: DATABASE_USERNAME,
    password: DATABASE_PASSWORD,
    database: DATABASE_NAME + "_development",
    host: DATABASE_HOST,
    dialect: "postgres",
  },
  test: {
    username: DATABASE_USERNAME,
    password: DATABASE_PASSWORD,
    database: DATABASE_NAME + "_test",
    host: DATABASE_HOST,
    dialect: "postgres",
  },
  production: {
    username: DATABASE_USERNAME,
    password: DATABASE_PASSWORD,
    database: DATABASE_NAME,
    host: DATABASE_HOST,
    dialect: "postgres",
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false
      }
    }
  },
};
