/**
 * @file
 * Router is defined in here.
 *
 * Every endpoint is described in this file.
 * It is done by defining request handler,
 * and it's middleware as well.
 */

/**
 * Dependencies */
const express = require("express");

/**
 * Initialize router instance */
const router = express.Router();

/**
 * Import controllers */
const mainController = require("./controllers/mainController");
const taskController = require("./controllers/taskController");

/**
 * Import middlewares */
const authorize = require("./middlewares/authorize");

/**
 * Endpoint definition */
router.get("/", mainController.handleGetRootRequest);

/**
 * Task resource routes */
router.get("/api/v1/tasks", authorize, taskController.handleGetTasksRequest);
router.post("/api/v1/tasks", authorize, taskController.handlePostTasksRequest);
router.get("/api/v1/tasks/:id", authorize, taskController.handleGetTaskRequest);
router.put("/api/v1/tasks/:id", authorize, taskController.handlePutTaskRequest);
router.delete(
  "/api/v1/tasks/:id",
  authorize,
  taskController.handleDeleteTaskRequest
);

router.use(mainController.handleNotFoundRequest);
router.use(mainController.handleExceptionRequest);

module.exports = router;
