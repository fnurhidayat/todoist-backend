/**
 * @file
 * HTTP Server is defined in here.
 *
 * In this file, logger, body parser, and router is injected
 * to the HTTP server as middleware. Where the logger only active
 * if only the server is not run inside test environment.
 */

/**
 * Dependencies */
const { initializeApp, cert } = require("firebase-admin/app");

/**
 * Fetch variable from process environment variable */
const {
  FIREBASE_PROJECT_ID,
  FIREBASE_PRIVATE_KEY,
  FIREBASE_CLIENT_EMAIL,
  FIREBASE_DATABASE_URL,
} = process.env;

const admin = initializeApp({
  credential: cert({
    projectId: FIREBASE_PROJECT_ID,
    private_key: FIREBASE_PRIVATE_KEY,
    client_email: FIREBASE_CLIENT_EMAIL,
  }),
  databaseURL: FIREBASE_DATABASE_URL,
});

module.exports = admin;
