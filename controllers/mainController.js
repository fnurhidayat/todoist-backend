/**
 * @file
 * Server generic response.
 */

/**
 * GET / */
function handleGetRootRequest(req, res) {
  res.status(200).json({
    status: "OK",
    data: {
      name: "Todoist API",
      message: "I'm Okay!",
    },
  });
}

/**
 * onLost */
function handleNotFoundRequest(req, res) {
  res.status(404).json({
    status: "FAIL",
    data: {
      name: "NotFoundError",
      message: "Are you lost?",
    },
  });
}

/**
 * onError */
function handleExceptionRequest(err, req, res, next) {
  res.status(500).json({
    status: "ERROR",
    data: {
      name: "InternalServerError",
      message: err.message,
      stack: err.stack,
    },
  });
}

module.exports = {
  handleGetRootRequest,
  handleNotFoundRequest,
  handleExceptionRequest,
};
