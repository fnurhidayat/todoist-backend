FROM node:14.16-alpine AS todoistd
LABEL maintainer=fnurhidayat@binar.co.id
ARG NODE_ENV=production
WORKDIR /opt/todoistd
COPY . .
RUN yarn
ENV PATH="$PATH:/opt/todoistd/node_modules/.bin" \
    NODE_ENV=${NODE_ENV} \
    PORT=8000
CMD ["/bin/sh", "-c", "yarn", "start"]
